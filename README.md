## Tutorial API publica documentanda com Swagger

# Resposta das questões do exercício

3.1) Localização da documentação do Web Services (endpoint): Esta Portal da transparência que dispõe de documentos do governo federal [portal da transparência](http://www.transparencia.gov.br/swagger-ui.html#!/Despesas32P250blicas/documentosUsingGET "Portal da Transparência");

3.2) Localização do WSDL ou Swagger ou outra forma de documentação de interface: [portal da transparência](http://www.transparencia.gov.br/swagger-ui.html#!/Despesas32P250blicas/documentosUsingGET "Portal da Transparência");

3.3) Uma descrição resumida dos serviços disponibilizados e a documentação dos exemplos;

* Acordos de Leniência
* Bolsa Família
* Cadastro de Expulsões da Administração Federal (CEAF)
* Cadastro Nacional de Empresas Inidôneas e Suspensas (CEIS)
* Cadastro Nacional de Empresas Punidas (CNEP)
* Contratos do Poder Executivo Federal
* Convênios do Poder Executivo Federal
* Despesas Públicas
* Entidades Privadas sem Fins Lucrativos Impedidas (CEPIM)
* Garantia-Safra
* Gastos por meio de cartão de pagamento
* Licitações do Poder Executivo Federal
* Órgãos
* Programa de Erradicação do Trabalho Infantil (Peti)
* Seguro Defeso
* Servidores do Poder Executivo Federal
* Viagens a serviço

Consulta qualquer documento do portal da transparência dos orgãos do governo federal.
# Passo a passo

1. Inicializando o projeto com package.json e as depêndencias iniciais
`npm init`

2. Acrescentar dependências no package.json e no script
```
    "express": "^4.17.0",
    "nodemon": "^1.19.1",
    "request": "^2.88.0"
```
3. Acrescentar dependências no package.json e no script
```
"start": "nodemon server.js"
```
 
4. Update package.json com `npm i`

7. Todo o codigo foi colocado no arquivo server.js o resultado da consulta é exibido no terminal
![server](img/server.png)  

## Consultas empenhos
1. São necessários os seguintes parâmetros na consulta da api de empenhos emitidos, caso não sejam informados será preenchido pelos da Advocacia-Geral da União dia 14 fevereiro 2019:
- unidadeGestora
- gestao
- dataEmissao
- fase
- pagina

## Consultas Orgãos
1. São necessários os seguintes parâmetros na consulta da api de orgãos, caso não seja informado será informado da Advocacia-Geral da União
- codigo
- pagina

## Consulta Remuneração Servidores
1. São necessários os seguintes parâmetros na consulta da api de orgãos, caso não seja informado será informado da Advocacia-Geral da União
- codigo
- pagina

## Teste realizado pelo postman
![Teste com params](img/postman1.png "Empenhos")  
![Teste sem params](img/postman2.png "Orgaos")  

